var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var cors = require('cors')



const IOST = require('iost')
// init iost sdk
const iost = new IOST.IOST({ // will use default setting if not set
  gasRatio: 1,
  gasLimit: 100000,
  expiration: 90,
});

const ACCOUNT = 'admin';
const RPC = new IOST.RPC(new IOST.HTTPProvider('http://127.0.0.1:30001'));

var app = express();
app.use(cors())

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.get('/getDiploma', (req, res) => {
  let txnHash = req.query.txnHash
  try{
    RPC.transaction.getTxByHash(txnHash).then(txnDetails => {
      console.log('Retrieved diploma from ', txnHash)
      let diploma = txnDetails["transaction"]["actions"][0]["data"]
      let diplomaDet = diploma.substring(1, diploma.length - 1 ).split(",")
      res.json({name: diplomaDet[0], degree: diplomaDet[1], authorized_by: diplomaDet[2]});
    })
  } catch(e){
    console.log(e);
  }
})

app.get('/getBalance', (req, res) => {
  let address = req.query.address;
  // E87cKWAnLidR1oT4LdvhfgmkzGnDNGfNLFo1HtmbqWnc
  try {
   RPC.blockchain.getBalance("iost", address).then(balance => {
     let check = JSON.stringify(balance).substring(1,).split(",")[0].split(':')
     res.send({balance: check[1]})
   })
  } catch(e){
    console.log('Get Balance: ', e)
  }
})

// give reward -- TO DO: Try to be specific
app.get('/giveReward', (req, res) => {

  let user = req.query.user;

  const tx = iost.transfer("iost", "admin", "ernpas", "0.01", "memo")

  iost.setAccount(ACCOUNT);
  iost.setRPC(RPC);
  iost.signAndSend(tx)
    .on("pending", console.log)
    .on("success", console.log)
    .on("failed", console.log);

  res.end();

 /*
  try {
    RPC.blockchain.getAccountInfo("admin", 0).then(signer => {
      let signer_hash = signer["permissions"]["active"]["items"][0]["id"]
      iost.setPublisher("admin", signer_hash);
      let handler = iost.callABI("ContractBLETt47LCGAcQGjY6iDGQRRLMoPL3F7tKoggsENZm9Do", "getReward", [user,  "10.000"]);

      handler
      .onPending(console.log)
      .onSuccess(console.log)
      .onFailed(console.log)
      .send()
      .listen(); // if not listen, only onPending or onFailed (at sending tx) will be called
  }) 
}
  catch(e){

  }
  */


  })


// sponsor scholarship
app.get('/sponsorScholar', (req, res) => {
  try {
   
  } catch(e){

  }

})

app.get('/test', (req,res) => {

  RPC.blockchain.getAccountInfo("admin", 0).then(info => {
    res.send(info["permissions"]["active"]["items"][0]["id"])
  })

  const tx = iost.transfer("iost", "admin", "ernpas", "1.000", "memo");

  
  iost.signAndSend(tx)
      .onPending(console.log)
      .onSuccess(console.log)
      .onFailed(console.log)
  
      
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
